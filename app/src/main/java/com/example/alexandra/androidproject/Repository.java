package com.example.alexandra.androidproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Repository {
    private SQLiteOpenHelper helper;
    private SQLiteDatabase db;

    public Repository(Context context, int version){
        helper=new QuizHelper(context,"QuizDataBase",null,version);

    }

    public void openConnection(){
        db=helper.getWritableDatabase();
    }

    public void closeConnection(){
        db.close();
    }

    //Insert methods
    public void insertStudent(Student s, String statut){
        ContentValues cv=new ContentValues();
        cv.put("name",s.getName());
        cv.put("surname",s.getSurname());
        cv.put("email",s.getEmail());
        cv.put("username",s.getUserName());
        cv.put("password",s.getPassword());
        cv.put("statut",statut);
        db.insert(Constants.USER_TABEL,null,cv);
    }
    public void insertProf(Professor p, String statut){
        ContentValues cv=new ContentValues();
        cv.put("name",p.getName());
        cv.put("surname",p.getSurname());
        cv.put("email",p.getEmail());
        cv.put("username",p.getUsername());
        cv.put("password",p.getPassword());
        cv.put("statut",statut);
        db.insert(Constants.USER_TABEL,null,cv);
    }
    public void insertAnswers(Answer a, Integer questionId){
        ContentValues cv=new ContentValues();
        cv.put("content",a.getContent());
        cv.put("corect",a.getCorrect());
        cv.put("questionID",questionId);
        db.insert(Constants.ANSWERS_TABEL,null,cv);
    }
    public void insertQuestions(Question q){
        ContentValues cv=new ContentValues();
        cv.put("content",q.getQuestionContent());
        db.insert(Constants.QUESTIONS_TABEL,null,cv);
    }
    public void insertTest(Test t,int userID){
        ContentValues cv=new ContentValues();
        cv.put("userID",userID);
        cv.put("status",t.isStatus());
        db.insert(Constants.TEST_TABEL,null,cv);
    }
    public void insertGrades(int grade, int userID,int testID){
        ContentValues cv=new ContentValues();
        cv.put("userID",userID);
        cv.put("grade",grade);
        cv.put("testID",testID);
        db.insert(Constants.GRADES_TABEL,null,cv);

    }
    public void updateStudent(String email,int ID){
        ContentValues cv=new ContentValues();
        cv.put("email",email);
        db.update(Constants.USER_TABEL, cv, "ID="+ID, null);
    }

    //Update methods
    public void updateProf(String email, int ID){
        ContentValues cv=new ContentValues();
        cv.put("email",email);
        db.update(Constants.USER_TABEL, cv, "ID="+ID, null);
    }

    public void updateQuestion(int testID){
        ContentValues cv=new ContentValues();
        cv.put("testID",testID);
        db.update(Constants.QUESTIONS_TABEL,cv,null,null);
    }

    public void updateTest(int status,int id){
        ContentValues cv=new ContentValues();
        cv.put("status",status);
        db.update(Constants.TEST_TABEL,cv,"id="+id,null);
    }

    //Delete methods
    public void deleteUsers(Integer ID){
        String where="ID=?";
        db.delete(Constants.USER_TABEL,where,new String[]{ID.toString()});
    }
    public void deleteGrade(Integer gradeId){
        String where="ID=?";
        db.delete(Constants.GRADES_TABEL,where,new String[]{gradeId.toString()});
    }

    //Select methods
    public List<Student> selectStudents(){
        Cursor c=db.query(Constants.USER_TABEL,new String[]{"name","surname","email","username","password"},"statut='STUDENT'",null,null,null,null);
        List<Student> sList=new ArrayList<>();
        if(c!=null && c.moveToFirst()){

            do{
                Student s=new Student(c.getString(0),c.getString(1),c.getString(2),c.getString(3),c.getString(4));
                sList.add(s);
            }
            while(c.moveToNext());
        }


        return sList;
    }
    public int selectUserID(String userName){
        Cursor c=db.query(Constants.USER_TABEL,new String[]{"ID"},"username=?",new String[]{userName},null,null,null);
        if(c!=null && c.moveToFirst()){
            do {
                int id = c.getInt(0);
                return id;
            }while(c.moveToNext());
        }
        return 0;
    }

    public List<Professor> selectProfessors(){
        Cursor c=db.query(Constants.USER_TABEL,new String[]{"name","surname","email","username","password"},"statut='PROFESSOR'",null,null,null,null);
        List<Professor> sList=new ArrayList<>();
        if(c!=null && c.moveToFirst()){

            do{
                Professor p=new Professor(c.getString(0),c.getString(1),c.getString(2),c.getString(3),c.getString(4));
                sList.add(p);
            }
            while(c.moveToNext());
        }


        return sList;
    }

    public List<Answer> selectAnswers(Integer questionId){
        Cursor c=db.query(Constants.ANSWERS_TABEL,new String[]{"id","content","corect"},"questionID="+questionId,null,null,null,null);
        List<Answer> aList=new ArrayList<>();
        if(c!=null && c.moveToFirst()){
            do{
                boolean corect=false;
                if(c.getInt(2)==1)
                    corect=true;

                Answer a=new Answer(c.getInt(0),c.getString(1),corect,questionId);
                aList.add(a);
            }while(c.moveToNext());
        }
    return aList;
    }

    public List<Question> selectQuestions(){
        Cursor c=db.query(Constants.QUESTIONS_TABEL,new String[]{"id","content","testID"},null,null,null,null,null);
        List<Question> questions=new ArrayList<>();
        if(c!=null && c.moveToFirst()){
            do{
                Question q=new Question(c.getInt(0),c.getString(1),null);
                questions.add(q);
            }while(c.moveToNext());
        }
        return questions;
    }

    public int selectLastTestID(){
        Cursor c=db.query(Constants.TEST_TABEL,new String[]{"id"},null,null,null,null,null);
        int ID=-1;
        if(c!=null && c.moveToFirst()){
            do{
                 ID=c.getInt(0);

            }while(c.moveToNext());
        }
        return ID;
    }

    public Test selectTest(int id){
        Cursor c=db.query(Constants.TEST_TABEL,new String[]{"status"},"id="+id,null,null,null,null);

        if(c!=null && c.moveToFirst()){
            do{
                Test t=new Test();
                 t.setStatus(c.getInt(0)==1?true:false);
                 return t;
            }while(c.moveToNext());

        }

        return null;
    }


    public List<Integer> selectGradesForStudent(int studentID){
        Cursor c=db.query(Constants.GRADES_TABEL,new String[]{"grade"},"userID="+studentID,null,null,null,null);
        List<Integer> grades= new ArrayList<>();
        if(c!=null && c.moveToFirst()){
            do{
                int grade=c.getInt(0);
                grades.add(grade);
            }while(c.moveToNext());

        }
        return grades;
    }

    public List<Integer> selectGradesForTest(int testID){
        Cursor c=db.query(Constants.GRADES_TABEL,new String[]{"grade"},"testID="+testID,null,null,null,null);
        List<Integer> grades= new ArrayList<>();
        if(c!=null && c.moveToFirst()){
            do{
                int grade=c.getInt(0);
                grades.add(grade);
            }while(c.moveToNext());

        }
        return grades;
    }

    public List<Integer> selectListTest(int userID){
        Cursor c=db.query(Constants.TEST_TABEL,new String[]{"ID"},"userID="+userID,null,null,null,null);
        List<Integer> listTest=new ArrayList<>();
        if(c!=null && c.moveToFirst()){
            do{
              listTest.add(c.getInt(0));

            }while(c.moveToNext());

        }

        return listTest;
    }



}
