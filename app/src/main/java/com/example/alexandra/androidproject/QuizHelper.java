package com.example.alexandra.androidproject;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class QuizHelper extends SQLiteOpenHelper {
    public QuizHelper( Context context,  String name,  SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+Constants.USER_TABEL+" (ID integer primary key autoincrement,name text not null,surname text not null,email text not null, username text not null, password text not null, statut text not null);");
        db.execSQL("create table "+Constants.GRADES_TABEL+" (ID integer primary key autoincrement,grade integer not null,userID integer,testID integer,FOREIGN KEY(userID) REFERENCES USERS(ID)," +
                "FOREIGN KEY(testID) REFERENCES TESTS(ID));");
        db.execSQL("create table "+Constants.TEST_TABEL+" (ID integer primary key autoincrement,status,userID integer,FOREIGN KEY(userID) REFERENCES USERS(ID));");
        db.execSQL("create table "+Constants.QUESTIONS_TABEL+" (ID integer primary key autoincrement, content text not null,testID integer,FOREIGN KEY(testID) REFERENCES TESTS(ID));");
        db.execSQL("create table "+Constants.ANSWERS_TABEL+" (ID integer primary key autoincrement, content text not null,corect numeric not null,questionID integer,FOREIGN KEY(questionID) REFERENCES QUESTIONS(ID));");
            }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists "+Constants.USER_TABEL);
        db.execSQL("drop table if exists "+Constants.GRADES_TABEL);
        db.execSQL("drop table if exists "+Constants.TEST_TABEL);
        db.execSQL("drop table if exists "+Constants.QUESTIONS_TABEL);
        db.execSQL("drop table if exists "+Constants.ANSWERS_TABEL);

        onCreate(db);
    }
}
