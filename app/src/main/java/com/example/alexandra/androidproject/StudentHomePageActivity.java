package com.example.alexandra.androidproject;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class StudentHomePageActivity extends AppCompatActivity {

    TextView  tvWelcome;
    Button btnViewGrades;
    Button btnTakeQuiz;
    String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_home_page);
        tvWelcome =findViewById(R.id.tvWelcome);
        btnViewGrades=findViewById(R.id.gradesButton);
        btnTakeQuiz=findViewById(R.id.takeQuizButton);
        Intent i=getIntent();
        username=i.getStringExtra("userName");
        tvWelcome.setText("Welcome, "+i.getStringExtra("userName"));
        Typeface myCustomFont= Typeface.createFromAsset(getAssets(),"fonts/Chalisa Octavia Font_D by 7NTypes.otf");
        tvWelcome.setTypeface(myCustomFont);

    btnViewGrades.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intentie4 = new Intent(StudentHomePageActivity.this, ViewGradesStudent.class);
            intentie4.putExtra("username",username);
            startActivity(intentie4);

        }
    });

        btnTakeQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentie4 = new Intent(StudentHomePageActivity.this, TakeQuizzActivity.class);
                intentie4.putExtra("username",username);
                startActivity(intentie4);

            }
        });
    }


}
