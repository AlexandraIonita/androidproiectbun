package com.example.alexandra.androidproject;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment implements  View.OnClickListener{

    TimePicker tp;
    int hour;
    int minutes;
    private OnFragmentListener mCallback;
    Button btnDone;
    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View  view= inflater.inflate(R.layout.fragment_blank, container, false);
        tp=view.findViewById(R.id.timePicker);
        btnDone=view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(this);
        return view;
    }

    public void setmCallback(OnFragmentListener mCallback) {
        this.mCallback = mCallback;
    }

    @Override
    public void onClick(View v) {
        hour=tp.getHour();
        minutes=tp.getMinute();
        mCallback.onBtnClick(hour+" "+minutes+" ");
    closefragment();}

    public interface OnFragmentListener
    {
        void onBtnClick(String msg);
    }
    private void closefragment() {
        getFragmentManager().beginTransaction().remove(this).commit();
    }
}
