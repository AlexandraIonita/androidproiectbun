package com.example.alexandra.androidproject;

public class Answer {

    private Integer id;
    private String content;
    private Boolean correct;
    private Integer questionId;

    public Answer(Integer id, String content, Boolean correct, Integer questionId) {
        this.id = id;
        this.content = content;
        this.correct = correct;
        this.questionId=questionId;
    }

    public Answer(Integer id, String content, Boolean correct) {
        this.id = id;
        this.content = content;
        this.correct = correct;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }


    @Override
    public String toString() {
        return this.id+"-"+this.content;
    }
}
