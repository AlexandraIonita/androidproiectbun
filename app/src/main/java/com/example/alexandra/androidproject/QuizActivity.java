package com.example.alexandra.androidproject;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

public class QuizActivity extends AppCompatActivity implements BlankFragment.OnFragmentListener{

    Button btnSetTime;
    Button btnCreateTest;
    String username;
    int userid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        Intent i=getIntent();
        username=i.getStringExtra("username");
        Repository repo=new Repository(getApplicationContext(),Constants.VERSION);
        repo.openConnection();
        userid=repo.selectUserID(username);
        repo.closeConnection();
        TextView tv=findViewById(R.id.tvQuestion);
        Quiz quiz=new Quiz(1,"test1",tv,getApplicationContext());


        btnSetTime=findViewById(R.id.btnSetTime);
        btnCreateTest=findViewById(R.id.btnCreateTest);

        quiz.execute("https://api.myjson.com/bins/pys1o");

    btnCreateTest.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Test t=new Test();
            Repository repo=new Repository(getApplicationContext(),Constants.VERSION);
            repo.openConnection();
            repo.insertTest(t,userid);
            int id=repo.selectLastTestID();
            repo.updateQuestion(id);
            repo.closeConnection();
        }
    });


    }

    public void setTime(View v){
        Fragment fragment;
        fragment=new BlankFragment();
        ((BlankFragment)fragment).setmCallback(this);
        FragmentManager fManager = getSupportFragmentManager();
        FragmentTransaction fTransaction = fManager.beginTransaction();
        fTransaction.replace(R.id.fragment, fragment);
        fTransaction.commit();
    }

    @Override
    public void onBtnClick(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


}
