package com.example.alexandra.androidproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ViewGradesStudent extends AppCompatActivity {

    Button btnExport;
    Button btnRead;
    TextView tvReadFile;
    private static final String FILE_NAME = "grades.txt";
    private TextView tv;
    String username;
    int userID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_grades_student);
        tv=findViewById(R.id.tvGrades1);

        Intent i=getIntent();
        username=i.getStringExtra("username");

        Repository repo=new Repository(getApplicationContext(),Constants.VERSION);
        repo.openConnection();
        userID=repo.selectUserID(username);
        List<Integer> list=repo.selectGradesForStudent(userID);
        repo.openConnection();

        StringBuilder s=new StringBuilder();

        for(int j=0;j<list.size();j++) {
            s.append(list.get(j).toString());
            s.append("\n");
        }

        tv.setText(s.toString());


        btnExport = findViewById(R.id.btnExport);
        btnRead = findViewById(R.id.btnRead);
        tvReadFile = findViewById(R.id.tvReadFile);
    }


    public void export(View v) throws FileNotFoundException {
        String text = tv.getText().toString();
        FileOutputStream fos = null;

        try {
            fos = openFileOutput(FILE_NAME, MODE_PRIVATE);
            fos.write(text.getBytes());

            Toast.makeText(this, "Saved to " + getFilesDir() + "/" + FILE_NAME, Toast.LENGTH_LONG).show();

        } catch (IOException e) {
            e.printStackTrace();



        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    public String readFile(String FILE_NAME) {
        String text = "";
        try {
            FileInputStream fis = openFileInput(FILE_NAME);
            int size = fis.available();
            byte[] buffer = new byte[size];
            fis.read(buffer);
            fis.close();
            text = new String(buffer);

        } catch (Exception e) {

            e.printStackTrace();
            Toast.makeText(ViewGradesStudent.this, "Error", Toast.LENGTH_LONG).show();

        }
        return text;
    }

}
