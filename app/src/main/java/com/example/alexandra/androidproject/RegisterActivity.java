package com.example.alexandra.androidproject;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends AppCompatActivity {

    Button btnRegister;
    CheckBox cb;
    EditText edtName;
    EditText edtSurname;
    EditText edtEmail;
    EditText edtUsername;
    EditText edtPassword;
    Spinner spinner;

    Button btnChoooseImage;
    ImageView imgProfile;
    private static final int PICK_IMAGE=100;
    Uri imageUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btnChoooseImage=findViewById(R.id.btnChooseImage);
        imgProfile=findViewById(R.id.imageProfile);


        final Spinner spinnerUT = findViewById(R.id.spinnerUserType);
        edtName=findViewById(R.id.registerName);
        edtSurname=findViewById(R.id.registerSurname);
        edtEmail=findViewById(R.id.registerEmail);
        edtPassword=findViewById(R.id.registerPassword);
        edtUsername=findViewById(R.id.registerUsername);




        btnChoooseImage.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                openGallery();
            }
        });




        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(RegisterActivity.this,
                R.array.user_types, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUT.setAdapter(adapter);
        btnRegister=findViewById(R.id.registerButton);
        cb=findViewById(R.id.cbAgree);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty((edtEmail).getText())||TextUtils.isEmpty((edtName).getText())||
                        TextUtils.isEmpty((edtPassword).getText())||TextUtils.isEmpty((edtSurname).getText())||TextUtils.isEmpty((edtUsername).getText())) {
                    if (TextUtils.isEmpty((edtEmail).getText())) {
                        edtEmail.setError("Insert value");

                    }
                    if (TextUtils.isEmpty((edtName).getText())) {
                        edtName.setError("Insert value");
                    }
                    if (TextUtils.isEmpty((edtPassword).getText())) {
                        edtPassword.setError("Insert value");
                    }
                    if (TextUtils.isEmpty((edtSurname).getText())) {
                        edtSurname.setError("Insert value");
                    }
                    if (TextUtils.isEmpty((edtUsername).getText())) {
                        edtUsername.setError("Insert value");
                    }
                }
                else{
                    if (cb.isChecked()) {


                        //inserare db

                        Repository repo = new Repository(getApplicationContext(), Constants.VERSION);
                        repo.openConnection();
                        if (spinnerUT.getSelectedItem().equals("STUDENT")) {
                            Student s = new Student(edtName.getText().toString(), edtSurname.getText().toString(), edtEmail.getText().toString(), edtUsername.getText().toString(), edtPassword.getText().toString());
                            repo.insertStudent(s, spinnerUT.getSelectedItem().toString());
                        } else if (spinnerUT.getSelectedItem().equals("PROFESSOR")) {
                            Professor p = new Professor(edtName.getText().toString(), edtSurname.getText().toString(), edtEmail.getText().toString(), edtUsername.getText().toString(), edtPassword.getText().toString());
                            repo.insertProf(p, spinnerUT.getSelectedItem().toString());
                        }
                        repo.closeConnection();
                        Toast.makeText(getBaseContext(), "Registered successfully", Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(RegisterActivity.this, LoginPageActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getBaseContext(), "Terms and conditions are mandatory", Toast.LENGTH_LONG).show();
                    }


                }

            }
        });






    }

    private void openGallery(){
        Intent gallery=new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery,PICK_IMAGE);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(resultCode== RESULT_OK && requestCode==PICK_IMAGE){
            imageUri=data.getData();
            imgProfile.setImageURI(imageUri);
        }


    }
}
