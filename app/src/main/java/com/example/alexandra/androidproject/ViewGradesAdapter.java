package com.example.alexandra.androidproject;

import android.content.Context;
import android.database.DataSetObserver;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class ViewGradesAdapter extends BaseAdapter {
    private ArrayList<StudentGrades> grades;
    ListAdapter listAdapter;
    private Context context;

    public ViewGradesAdapter(ArrayList<StudentGrades> grades, Context context) {
        this.grades = grades;
        this.context = context;
    }

    @Override
    public int getCount() {
        return grades.size();
    }

    @Override
    public Object getItem(int position) {
        return grades.get(position);
    }

    @Override
    public long getItemId(int position) {
        return grades.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
            convertView=LayoutInflater.from(context).inflate(R.layout.student_viewgrades,parent,false);
        StudentGrades grade=(StudentGrades)getItem(position);
        TextView tvTestID=convertView.findViewById(R.id.tvitem);
        TextView tvGrade=convertView.findViewById(R.id.tvGrade);

        tvTestID.setText(grade.getQuizName());
        tvGrade.setText("Nota:"+grade.getGrade());

        return convertView;

    }





}
