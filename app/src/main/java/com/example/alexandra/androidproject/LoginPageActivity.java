package com.example.alexandra.androidproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class LoginPageActivity extends AppCompatActivity {

    EditText edtUser;
    EditText edtPassword;
    CheckBox cb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);


        Button loginButton = findViewById(R.id.btnLogin);
        Button aboutButton = findViewById(R.id.btnAbout);
        Button registerButton = findViewById(R.id.btnRegister);
        Button loginAsProfessor = findViewById(R.id.btnLoginProf);
        edtUser=findViewById(R.id.edtUsername);
        edtPassword=findViewById(R.id.edtPassword);
        cb=findViewById(R.id.cbLogin);




        getUser();
        //
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentie = new Intent(LoginPageActivity.this, AboutActivity.class );
                startActivity(intentie);
            }
        });


        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent startRegisterActivity = new Intent(LoginPageActivity.this, RegisterActivity.class);
                startActivity(startRegisterActivity);
            }
        });


        //Login button for student
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean gasit=checkUser();
                if(gasit) {
                    if(cb.isChecked()) {
                        saveUser(v);
                    }
                    Intent startsStudentHomePageActivity = new Intent(LoginPageActivity.this, StudentHomePageActivity.class);
                    startsStudentHomePageActivity.putExtra("userName", edtUser.getText().toString());
                    startActivity(startsStudentHomePageActivity);
                }
                else{
                    Toast.makeText(getBaseContext(), "Incorrect username or password",Toast.LENGTH_LONG).show();
                }

            }

        });



        //Login button for professor
        loginAsProfessor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean gasit=checkUser2();
                if(gasit) {
                if (cb.isChecked()) {
                     saveUser(v);
                }
                    Intent startsProfessorHomePageActivity = new Intent(LoginPageActivity.this, ProfessorHomePageActivity.class);
                    startsProfessorHomePageActivity.putExtra("userName", edtUser.getText().toString());
                    startActivity(startsProfessorHomePageActivity);
                }
                else{
                    Toast.makeText(getBaseContext(), "Incorrect username or password",Toast.LENGTH_LONG).show();
                }

            }
        });



    }

    //method to save the user ->Sp
    protected void saveUser(View v){
        SharedPreferences sp=getSharedPreferences("setter",MODE_PRIVATE);
        SharedPreferences.Editor editor=sp.edit();
        String username=edtUser.getText().toString();
        editor.putString("username",username);
        String password=edtPassword.getText().toString();
        editor.putString("password",password);
        editor.commit();
    }
    //method to get the user->Sp

    protected void getUser(){
        SharedPreferences sp=getSharedPreferences("setter",MODE_PRIVATE);
        edtUser.setText(sp.getString("username",""));
        edtPassword.setText(sp.getString("password",""));
        //Toast.makeText(this,sp.getString("username",""),Toast.LENGTH_SHORT).show();
    }

    protected boolean checkUser(){
        Repository repo=new Repository(getApplicationContext(),Constants.VERSION);
        boolean gasit=false;
        repo.openConnection();
        List<Student> sList=repo.selectStudents();
        for(int i=0;i<sList.size();i++)
        {
            if(edtUser.getText().toString().equals(sList.get(i).getUserName()))
            {

                gasit=true;

            }
        }
        repo.closeConnection();

       return gasit;
    }
    protected boolean checkUser2(){
        Repository repo=new Repository(getApplicationContext(),Constants.VERSION);
        boolean gasit=false;
        repo.openConnection();
        List<Professor> sList=repo.selectProfessors();
        for(int i=0;i<sList.size();i++)
        {
            if(edtUser.getText().toString().equals(sList.get(i).getUsername()))
            {

                gasit=true;

            }
        }
        repo.closeConnection();

        return gasit;
    }



}
