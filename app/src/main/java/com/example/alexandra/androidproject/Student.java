package com.example.alexandra.androidproject;

import java.util.ArrayList;
import java.util.HashMap;

public class Student {
    private String name;
    private String surname;
    private String email;
    private String userName;
    private String password;
    private ArrayList<StudentGrades> grades;


    public Student(String name, String surname, String email, String userName,String password) {
        this.name = name;
        this.surname = surname;
        this.userName=userName;
        this.email = email;
        this.password = password;
        this.grades=new ArrayList<>();
    }

    public ArrayList<StudentGrades> getGrades() {
        return grades;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setGrades(ArrayList<StudentGrades> grades) {
        this.grades = grades;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString() {
        return super.toString();
    }

public void addGrade( StudentGrades grade ){
        this.grades.add(grade);
}

}
