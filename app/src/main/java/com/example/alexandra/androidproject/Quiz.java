package com.example.alexandra.androidproject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Quiz extends AsyncTask<String,Void,String> {

    private Integer id;
    private String title;
    private ArrayList<Question> questionList ;
    private float[] grades;
    private Student[] students;
    private Context context;
    TextView tv;

    public Quiz(Integer id, String title, TextView tv,Context context) {
        this.id = id;
        this.title = title;
        this.tv=tv;
        this.context=context;
        //this.questionList = questionList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Question> getQuestionList() {
        return questionList;
    }


    @Override
    public String toString() {
        return super.toString();
    }

    public void setQuestionList(ArrayList<Question> questionList) {
        this.questionList = questionList;
    }

    @Override
    protected String doInBackground(String... strings) {
        String result="";
        try{
            URL url=new URL(strings[0]);
            HttpURLConnection http=(HttpURLConnection)url.openConnection();
            InputStream stream=http.getInputStream();
            InputStreamReader rea=new InputStreamReader(stream);
            BufferedReader reader=new BufferedReader(rea);
            StringBuilder builder=new StringBuilder();

            String line="";
            while((line=reader.readLine())!=null) {
                builder.append(line);
            }

            JSONObject object=new JSONObject(builder.toString());
            JSONArray questionArray= (JSONArray)object.getJSONArray("quiz");
            questionList=new ArrayList<>();
            for(int i=0; i<questionArray.length();i++) {
                JSONObject obj=(JSONObject)questionArray.get(i);
                int id=obj.getInt("id");

                String content=obj.getString("questionContent");

                JSONArray answerArray=obj.getJSONArray("possibleAnswers");

                ArrayList<Answer> answerList=new ArrayList<>();

                for(int j=0;j<answerArray.length();j++) {
                    JSONObject obj1=(JSONObject)answerArray.get(j);
                    int id1=obj1.getInt("id");

                    String content1=obj1.getString("content");

                    Boolean correct=obj1.getBoolean("correct");
                    answerList.add(new Answer(id1,content1,correct));

                }

                Question question=new Question(id,content,answerList);
                questionList.add(question);
                }


           result="";
            for(int i=0;i<questionList.size();i++)
            {
                result+=questionList.get(i).toString();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();


        }

        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        this.tv.setText(s);
       Repository repo = new Repository(this.context, Constants.VERSION);

        repo.openConnection();
        List<Question> qq=repo.selectQuestions();
        if(qq.size()==0){
        for(int i=0;i<this.getQuestionList().size();i++) {

                repo.insertQuestions(this.questionList.get(i));

                for (int j = 0; j < this.getQuestionList().get(i).getPossibleAnswers().size(); j++)
                    repo.insertAnswers(this.getQuestionList().get(i).getPossibleAnswers().get(j), this.getQuestionList().get(i).getId());

        }
        repo.closeConnection();
    }}
}
