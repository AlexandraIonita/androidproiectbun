package com.example.alexandra.androidproject;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Question  {

    private Integer id;
    private String questionContent;

    public Question(Integer id, String questionContent, List<Answer> possibleAnswers) {
        this.id = id;
        this.questionContent = questionContent;
        this.possibleAnswers = possibleAnswers;
    }

    //Each question has multiple possible answers
    private List<Answer> possibleAnswers = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public List<Answer> getPossibleAnswers() {
        return possibleAnswers;
    }

    public void setPossibleAnswers(List<Answer> possibleAnswers) {
        this.possibleAnswers = possibleAnswers;
    }

    @Override
    public String toString() {
        StringBuilder result=new StringBuilder();
        result.append(this.id+"-"+this.questionContent);
        result.append("\n");
        for(int i=0;i<this.possibleAnswers.size();i++) {
            result.append(this.possibleAnswers.get(i).toString());
            result.append("\n");
        }
        return(result.toString());
    }
}
