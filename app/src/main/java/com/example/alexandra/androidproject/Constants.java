package com.example.alexandra.androidproject;

public class Constants {
    public static final String ANSWERS_TABEL = "ANSWERS_TABLE";
    static final String USER_TABEL="USERS";
    static final String GRADES_TABEL="GRADES";
    static final String TEST_TABEL="TESTS";
    static final String QUESTIONS_TABEL="QUESTIONS";
    static final int VERSION=5;

}
