package com.example.alexandra.androidproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import java.util.List;

public class CheckAdapter extends BaseAdapter
{
    List<Answer> answer;
    Context context;
    LayoutInflater inflater;
    Answer a=null;


    public CheckAdapter(Context context,  List<Answer>  answers){
        this.answer=answers;
        this.context=context;
        inflater=(LayoutInflater.from(context));

    }
    @Override
    public int getCount() {
        return answer.size();
    }
    public Answer getCheckItem(){
        return this.a;

    }

    @Override
    public Object getItem(int position) {
        return answer.get(position);
    }

    public List<Answer> getAnswer() {
        return answer;
    }

    public void setAnswer(List<Answer> answer) {
        this.answer = answer;
    }

    @Override
    public long getItemId(int position) {
        return answer.get(position).getId();

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
       convertView=inflater.inflate(R.layout.checktextviewlayout,null);
        final CheckedTextView simpleCheck=(CheckedTextView)convertView.findViewById(R.id.checkView);
        simpleCheck.setText(answer.get(position).getContent());
        simpleCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(simpleCheck.isChecked()){
                    simpleCheck.setCheckMarkDrawable(0);
                    simpleCheck.setChecked(false);
                }
                else{
                    simpleCheck.setCheckMarkDrawable(R.drawable.checked);
                    simpleCheck.setChecked(true);
                    a=answer.get(position);
                }
            }
        });
        return convertView;
    }
}
