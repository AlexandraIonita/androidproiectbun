package com.example.alexandra.androidproject;

public class StudentGrades {
    private int id;
    private String quizName;
    private float grade;


    public StudentGrades(int ID,String quizName, float grade) {
        this.id=ID;
        this.quizName = quizName;
        this.grade = grade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }


}
