package com.example.alexandra.androidproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import static android.view.animation.Animation.INFINITE;

public class TakeQuizzActivity extends AppCompatActivity {
    ProgressBar pb;
    Button btnNext;
    TextView tvQ;
    TextView tvA;
    List<Question> questionList;
    List<Answer> answerList;
    ListView lv;
    Repository repo;
    String username;
    int score=0;
    int userID;
    LinearLayout ll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_quizz);

        pb=findViewById(R.id.pb);
        tvQ=findViewById(R.id.tvQuestion);
        ll=findViewById(R.id.myll);
        lv=(ListView)findViewById(R.id.lvAnswers);
        btnNext = findViewById(R.id.btnNext);
        repo=new Repository(getApplicationContext(),Constants.VERSION);
        Intent i=getIntent();
        username=i.getStringExtra("username");
        repo.openConnection();
        userID=repo.selectUserID(username);
        repo.openConnection();
        final int id=repo.selectLastTestID();
        final Test t=repo.selectTest(id);
        repo.updateTest(1,id);
        t.setStatus(true);
        if(t!=null && t.isStatus()) {

            questionList=repo.selectQuestions();
            pb.setMax(questionList.size());
            pb.setProgress(0);
            tvQ.setText(questionList.get(0).getQuestionContent());
            answerList=repo.selectAnswers(1);
            final CheckAdapter adapter=new CheckAdapter(getApplicationContext(),answerList);
            lv.setAdapter(adapter);

            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Answer chosenAnswer=adapter.getCheckItem();
                    if(chosenAnswer.getCorrect()==true)
                        score+=10;
                    if(pb.getProgress()==(pb.getMax()-1)){
                        repo.openConnection();
                        repo.insertGrades(score,userID,id);
                        repo.closeConnection();
                        Intent i=new Intent(TakeQuizzActivity.this,ViewGradesStudent.class);
                        i.putExtra("username",username);
                        startActivity(i);

                    }
                    repo.openConnection();
                    answerList=repo.selectAnswers(questionList.get(pb.getProgress()).getId()+1);
                    pb.incrementProgressBy(1);
                    tvQ.setText(questionList.get(pb.getProgress()).getQuestionContent());
                    adapter.setAnswer(answerList);
                    lv.setAdapter(adapter);
                    repo.closeConnection();


                }
            });
        }

        repo.closeConnection();
    }





}
