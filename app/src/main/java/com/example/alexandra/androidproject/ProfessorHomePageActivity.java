package com.example.alexandra.androidproject;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ProfessorHomePageActivity extends AppCompatActivity {

    TextView tv;
    Button btnViewGrades;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professor_home_page);
        tv=findViewById(R.id.tvWelcomProf);
        btnViewGrades=findViewById(R.id.viewGradesButton);
        Typeface myCustomFont= Typeface.createFromAsset(getAssets(),"fonts/Chalisa Octavia Font_D by 7NTypes.otf");
        tv.setTypeface(myCustomFont);
        Intent i=getIntent();
        final String username;

        tv.setText("Welcome, "+i.getStringExtra("userName").toString());
        username=i.getStringExtra("username");
        Button btnCreateQuiz=findViewById(R.id.createQuizButton);

        btnCreateQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentie=new Intent(ProfessorHomePageActivity.this, QuizActivity.class);
                intentie.putExtra("username",username);
                startActivity(intentie);
            }
        });

        btnViewGrades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentie=new Intent(ProfessorHomePageActivity.this, ViewGradesProfessorActivity.class);
                intentie.putExtra("username",username);
                startActivity(intentie);
            }
        });
    }
}
