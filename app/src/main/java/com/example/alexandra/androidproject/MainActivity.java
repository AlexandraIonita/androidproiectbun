package com.example.alexandra.androidproject;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button welcomeButton = findViewById(R.id.mainActivityButton);
        Typeface myCustomFont= Typeface.createFromAsset(getAssets(),"fonts/GROBOLD.ttf");
        welcomeButton.setTypeface(myCustomFont);
        welcomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentie2 = new Intent(MainActivity.this, LoginPageActivity.class);
                startActivity(intentie2);
            }
        });




    }
}
