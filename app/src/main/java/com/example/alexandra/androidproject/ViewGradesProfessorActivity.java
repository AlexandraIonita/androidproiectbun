package com.example.alexandra.androidproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class ViewGradesProfessorActivity extends AppCompatActivity {

    TextView tv;
    String username;
    int userid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_grades_professor);
        tv=findViewById(R.id.tvGradesProf);

        Intent i=getIntent();
        username=i.getStringExtra("username");
        Repository repo=new Repository(getApplicationContext(),Constants.VERSION);
        repo.openConnection();
        userid=repo.selectUserID(username);
        List<Integer> tests=repo.selectListTest(userid);
        StringBuilder s=new StringBuilder();
        for(int j=0;j<tests.size();j++)
        {
            //Find all the gardes

            List<Integer> grades=repo.selectGradesForTest(tests.get(j));
            s.append(tests.get(j).toString());
            s.append("\n");
            for(int k=0;k<grades.size();k++)
            {
                s.append("\t");
                s.append(grades.get(k));
                s.append("\n");
            }

        }
        repo.closeConnection();

        tv.setText(s.toString());
    }
}
